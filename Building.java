import java.util.*;

public class Building {
		public float height;
		public int nFloor;
		public float area;
		public String companyName;
		public float areaCost;
		
		public Building(float h, int nF, float aR, String cN, float aC) {
			
			height = h;		// feet
			nFloor = nF;		
			area = aR;		//in sq feet
			companyName = cN;  
			areaCost = aC;
		}
		public float aCost()
		{  
			float totalAreacost = area * areaCost;
			return totalAreacost;
		}
		public float getheight(){return height;};
		public int getnFloor(){return nFloor;};
		public float getarea(){return area;};
		public String getcname(){return companyName;};
		public float getareacost(){return areaCost;};
		//public int getDate() {return Date;};
		void details() {
			System.out.println((building1.getcname()) + " are providing new flats  with building Height " 
					+ (building1.getheight()) + " feet ,No of Floors " + (building1.getnFloor()) 
					+ " ,Area per flat " + (building1.getarea()) + " feet. The area cost is Rs. " 
					+ (building1.getareacost()) + " with Total Area cost to be Rs. %f",building1.aCost);
					
		}
		
		public static void main(String[] args)
		{
			Building building1 = new Building(149.5f, 10, 1200.50f, "Ritik Group", 24999.99f);
			//Date d1 = new Date();
			building1.details();
			//d1.details();
		}
}
