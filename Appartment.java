
public class Appartment {
	public int emptyHouses, totalHouses;
	public float rentPerhouse;
	
	public Appartment(int eh, int th, float rph)
	{
		emptyHouses = eh;
		totalHouses = th;
		rentPerhouse = rph;
	}
	
	public int getemptyH(){return emptyHouses;};
	public int getTotalH(){return totalHouses;};
	public float getrentPerH(){return rentPerhouse;};
	
	public int occupiedHouseCount() {
		int occupiedHouse = totalHouses - emptyHouses;
		return occupiedHouse;
	}
	public float TotalIncome() {
		float income = this.occupiedHouseCount() * rentPerhouse;
		return income;
	}
	public static void main(String[] args) {
		Appartment appartment1 = new Appartment(10,30,24999.99f);
		
		System.out.println("No. of Empty Houses = " +appartment1.getemptyH());
		System.out.println("Total No. of Houses = " +appartment1.getTotalH());
		System.out.println("Rent Per House = Rs." +appartment1.getrentPerH() + " per month");
		System.out.println("Number of Ocuupied Houses = " +appartment1.occupiedHouseCount());
		System.out.println("Total Income = " +appartment1.TotalIncome());
	}
}
